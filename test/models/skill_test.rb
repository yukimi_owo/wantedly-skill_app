require 'test_helper'

class SkillTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @skill = @user.skills.build(name: "Skill A", count: 0)
  end
  
  test "should be valid" do
    assert @skill.valid?
  end
  
  test "user id should be present" do
    @skill.user_id = nil
    assert_not @skill.valid?
  end
  
  test "counter should be positive value" do
    @skill.count = -1
    assert_not @skill.valid?
  end
  
  test "order should be most first" do
    max_count = Skill.maximum('count')  #これは問題ない
    assert_equal Skill.find_by(count: max_count), Skill.first  #skillsからcount=5がとりだせていない
    #同じ数字の場合の処理
  end
end
