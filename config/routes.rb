Rails.application.routes.draw do
  get 'sessions/new'

  root 'static_pages#home'
  get    '/help',    to: 'static_pages#help'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  resources :skills,  only: [:update]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
