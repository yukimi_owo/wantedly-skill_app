# Wantedlyのスキルページ再現のためのアプリケーション

[Wantedlyの選考課題](https://gist.github.com/luvtechno/10ffa6e8f808cf5d8b4e3073087a08f9)のアプリケーションです。

##使い方
ナビゲーションのHelpをクリックしてもらえれば使い方がかいてあります。

* Log inを押してログインする
* * ユーザ1人目はemail = "example@railstutorial.org", password = "foobar"
* * ユーザ2人目以降はemail = "example-n@railstutorial.org"(nには1〜100の数字), password = "password"
* ログイン後、Usersからユーザ一覧を、Account→Profileから自分のプロフィールを見ることができる
* User一覧からユーザの名前をクリックするとそのユーザのプロフィール画面にうつる
* 小さく並んでいるスキル名をクリックするとスキルが追加され、クリックされた回数とともにリストになって表示される
* すでに追加されているものはクリックされた回数の部分をクリックすると回数を増やすことができる
* 一度クリックしたスキルはもう一度クリックしてもなにも起こらないようになっている
* Account→Log outでログアウトすることができる

Rails Tutorialに取り組み、それにしたがってやったため、仕様はRails Tutorialのものと近いです。

ユーザの定義などが行われていなかったため、ユーザ情報とスキル情報は自分で生成しました。

chromeとsafariでの動作は確認済みです。