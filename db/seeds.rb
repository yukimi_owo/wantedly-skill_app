User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(20)
#emailでよびだす…？
20.times do |n|
  sname = "Skill #{n+1}"
  users.each { |user| user.skills.create!(name: sname, count: 0) }
end