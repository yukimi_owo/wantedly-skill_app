class CreateFootprints < ActiveRecord::Migration[5.0]
  def change
    create_table :footprints do |t|
      t.integer :skill_id
      t.integer :user_id

      t.timestamps
    end
  end
end
