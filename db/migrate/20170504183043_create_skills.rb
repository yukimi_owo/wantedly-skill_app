class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.string :name
      t.integer :count
      t.references :user, foreign_key: true

      t.timestamps
    end
      add_index :skills, [:user_id, :count]
  end
end
