class AddFootprintIdToFootprints < ActiveRecord::Migration[5.0]
  def change
    add_column :footprints, :footprint_id, :integer
  end
  add_index :footprints, [:user_id, :skill_id]  #つけたし
end
