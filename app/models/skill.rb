class Skill < ApplicationRecord
  belongs_to :user
  default_scope -> { order(count: :desc) }
  validates :user_id, presence: true
  validates :count, presence: true, numericality: {only_integer: true, greater_than_or_equal_to: 0 }
end
