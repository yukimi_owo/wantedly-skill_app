class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index]
  
  def index
    @users = User.paginate(page: params[:page])
  end

  
  def show
    @user = User.find(params[:id])
    @skills = @user.skills.all
    @footprints = @user.footprints.all
  end
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    # beforeアクション


    # 正しいユーザーかどうか確認
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
end
