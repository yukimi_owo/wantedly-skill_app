class SkillsController < ApplicationController
  before_action :logged_in_user, only: [:update]
  
  def update
    @skill = Skill.find(params[:id])
    @user = User.find_by(id: @skill.user.id)
    @footprint = Footprint.find_by(user_id: @user.id, footprint_id: current_user.id, skill_id: @skill.id)
    if !!@footprint
      redirect_to user_path(@skill.user_id)
    else
      if @skill.increment!(:count, 1)
        @footprint = @user.footprints.build(footprint_id: current_user.id, skill_id: @skill.id)
        @footprint.save
        redirect_to user_path(@skill.user_id)
      else
        redirect_to user_path(@skill.user_id)
      end
    end
  end
end
